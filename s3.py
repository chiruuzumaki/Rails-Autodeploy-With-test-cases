import boto3
import pickle
from botocore.exceptions import ClientError

class S3:
    def __init__(self,env):
        self.env=env
        self.download_old_ami_name()
        pass

    def upload_new_ami(self,ami):
#            print(example)
            self.data['ami']=ami
            self.data['version']=self.env.versionname(self.data['version'])
            pickle_out = open(self.env.file,"wb")
            pickle.dump(self.data, pickle_out)
            pickle_out.close()
            bucketName=self.env.bucket
            Key=self.env.file
            outPutname=self.env.file
            s3 = boto3.client('s3')
            s3.upload_file(Key,bucketName,outPutname)
            
    def download_old_ami_name(self):
        try:
            sclient = boto3.client('s3')
            response = sclient.get_object(Bucket=self.env.bucket, Key=self.env.file)
            body_string = response['Body'].read()
            positive_model_data = pickle.loads(body_string)
            self.data=positive_model_data
        except ClientError:
                positive_model_data={}
                positive_model_data['ami']=self.env.ami
                positive_model_data['Image_name']=self.env.Image_name
                positive_model_data['loadbalancer']=self.env.loadbalancer
                positive_model_data['tag']=self.env.tag
                positive_model_data['sec']=self.env.sec
                positive_model_data['instance_size']=self.env.instance_size
                positive_model_data['VolumeSize']= self.env.VolumeSize
                positive_model_data['ssh_key']=self.env.ssh_key
                self.data=positive_model_data