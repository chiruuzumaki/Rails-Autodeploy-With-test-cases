import git
import os
import subprocess
import psutil
import time
import notify2
class Git:
    def __init__(self,env):
        self.env=env
        pass
    def checkIfProcessRunning(self,processName):

        for proc in psutil.process_iter():
            try:
                if processName.lower() in proc.name().lower():
                    return True
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass
        return False

    def sendmessage(self,title, message):
        notify2.init("deploy")
        notice = notify2.Notification(title, message)
        notice.show()
        return

    def git_conf(self):
        try:
            path=self.env.path
            repo = git.Repo( path )
            repo.git.checkout(self.env.branch)
            master = repo.head.reference
            name=master.commit.author.name
            status=repo.git.pull('origin',self.env.branch)
            master = repo.head.reference
            name=master.commit.author.name
            if status=='Already up to date.':
                self.status=False
            else:
                    self.sendmessage('deploying',self.env.tag)
                    self.status=True
        except  Exception:
                    print('error')
        
