import subprocess
import configparser
import pickle
import docker
from docker.errors import ContainerError
class Deploy:
    def __init__(self,ec2,env):
        self.env=env
        self.ec2=ec2
        self.deploy_rails()
        pass
    
    def deploy_rails(self):
                try:
                    self.testcases()
                    std_out=subprocess.check_output('cd '+self.env.path+' && yarn',stderr=subprocess.STDOUT,shell=True)
                    std_out=subprocess.check_output('cd '+self.env.path+' && bundle',stderr=subprocess.STDOUT,shell=True)
                    std_out=subprocess.check_output('cd '+self.env.path+' && ./bin/webpack',stderr=subprocess.STDOUT,shell=True)
                    std_out=subprocess.check_output('cd '+self.env.path+' &&  bundle exec cap production deploy server='+self.env.main_ip,stderr=subprocess.STDOUT,shell=True)
                    std_out=subprocess.check_output('cd '+self.env.path+' &&  bundle exec cap production deploy server='+self.ec2.host.public_ip_address,stderr=subprocess.STDOUT,shell=True)
                    std_out=subprocess.check_output('cd '+self.env.path+' && git stash',stderr=subprocess.STDOUT,shell=True)
                    image,name=self.ec2.image_create(self.ec2.host.id,self.env.old)
                    self.ec2.delete(self.ec2.host.id)
                    
                except subprocess.CalledProcessError as e:
                            print(str(e.output))
                            self.ec2.delete()
                except ContainerError as e:
                            print(str(e.stderr))
                            self.ec2.delete()
    def testcases(self):
        client = docker.from_env()  
        volume={self.env.path: {'bind': '/root/webapp/'}}
        client.containers.run('chiruuzumaki/rails_run_cases', 'bash /root/run.sh',volumes=volume)
     
            