import boto3
import time

class Ec2:
        def __init__(self,env,s3):
            self.env=env
            self.s3=s3
            while self.get_by_name():
                time.sleep(60)
            self.create_ec2()
        
        def create_ec2(self):
            ec2 = boto3.resource('ec2')
            instance = ec2.create_instances(ImageId = self.env.ami
                ,MinCount = 1
                ,MaxCount = 1,
                InstanceType = self.env.instance_size,
                BlockDeviceMappings=[
                {
                    'DeviceName': '/dev/sda1',

                    'Ebs': {
                        
                        'VolumeSize':  self.env.volumeSize,
                    },
                },
            ],
            KeyName = self.env.ssh_key,
            SecurityGroupIds=[
                        self.env.sec,
                    ]
                    )
            host = instance[0]
            print(host.id)
            ec2 = boto3.client('ec2')
            ec2.create_tags(Resources=[host.id], Tags=[{'Key':'Name', 'Value':'dev_'+self.env.tag+''}])
            host.wait_until_running()
            ec2 = boto3.resource('ec2')
            host = ec2.Instance(host.id)        
            self.host= host

        def get_by_name(self):
            ec2 = boto3.client('ec2')
            filters = [{
                    'Name': 'tag:Name',
                    'Values': ['scale_'+self.env.tag]
                        },
                        {
                    'Name': 'instance-state-name',
                    'Values': ['running','pending']
                        },
                        ]
            reservations = ec2.describe_instances(Filters=filters)
            if len(reservations['Reservations'])>=1:
                return True
            else:
                return False

        def image_create(self,instance_id,old):
                ec2 = boto3.resource('ec2')
                host = ec2.Instance(instance_id)
                newname=self.env.versionname(old)
                image = host.create_image(Name=newname)
                print(image.id)
                self.image_wait(image.id)
                return image.id,newname
        def image_wait(self,image):
                ec2 = boto3.client('ec2')
                waiter = ec2.get_waiter('image_available')
                waiter.wait(ImageIds=[image.id])
                self.s3.upload_new_ami(image.id)        
        def delete(self):
            ec2 = boto3.client('ec2')
            ec2.terminate_instances(InstanceIds=[self.host.id])
            self.amidel(self.env.ami)

        def amidel(self,old):
            ami=old['ami']
            ec2 = boto3.client('ec2')
            snapid=ec2.describe_images(ImageIds=[ami])['Images'][0]['BlockDeviceMappings'][0]['Ebs']['SnapshotId']
            response = ec2.deregister_image(
                ImageId=ami
            )
            self.snapdel(snapid)

        def snapdel(self,snapid):
            ec2 = boto3.client('ec2')
            response = ec2.delete_snapshot(
            SnapshotId=snapid
            )
