import configparser
from s3 import S3
from ec2 import Ec2
from deployapp import Deploy
from gitrun import Git
import time
class Env:
       def __init__(self,config_main):
            self.load(config_main)
       def versionname(self,name):
            name,ver, rev = str(name).split('.')
            versionname=name+'.'+ver + '.' + str(int(rev)+1) 
            return versionname
       def load(self,config_main):
            config = configparser.ConfigParser()
            config.read('conf.ini')        
            self.bucket=config[config_main]['bucket']
            self.sec=config[config_main]['sec']
            self.path=config[config_main]['path']
            self.file=config[config_main]['file']
            self.main_ip=config[config_main]['main_ip']
            self.instance_size=config[config_main]['instance_size']
            self.VolumeSize=config[config_main]['VolumeSize']
            self.tag=config[config_main]['tag']
            self.ami=config[config_main]['ami']
            self.Image_name=config[config_main]['Image_name']
            self.loadbalancer=config[config_main]['Loadbalancer']
            self.ssh_key=config[config_main]['ssh_key']
            self.branch=config[config_main]['branch']
            git=Git(self)
            if git.status:
                    s3=S3(self)
                    self.ami=s3.data['ami']
                    ec2=Ec2(self,s3)
                    deploy=Deploy(ec2,self)
                    git.sendmessage('deployed',self.tag)