import pickle
import boto3
import configparser
import subprocess
import time
from botocore.exceptions import ClientError
from docker.errors import ContainerError

import docker
class Ec2:
    def __init__(self,config_main):
        config = configparser.ConfigParser()
        config.read('greconf.ini')        
        self.bucket=config[config_main]['bucket']
        self.sec=config[config_main]['sec']
        self.path=config[config_main]['path']
        self.file=config[config_main]['file']
        self.main_ip=config[config_main]['main_ip']
        self.instance_size=config[config_main]['instance_size']
        self.VolumeSize=config[config_main]['VolumeSize']
        self.tag=config[config_main]['tag']
        self.ami=config[config_main]['ami']
        self.Image_name=config[config_main]['Image_name']
        self.loadbalancer=config[config_main]['Loadbalancer']
        self.ssh_key=config[config_main]['ssh_key']
        pass

    def download_old_ami_name(self):
        try:
            sclient = boto3.client('s3')
            response = sclient.get_object(Bucket=self.bucket, Key=self.file)
            body_string = response['Body'].read()
            positive_model_data = pickle.loads(body_string)
            return positive_model_data
        except ClientError:
                positive_model_data={}
                positive_model_data['ami']=self.ami
                positive_model_data['Image_name']=self.Image_name
                positive_model_data['loadbalancer']=self.loadbalancer
                positive_model_data['tag']=self.tag
                positive_model_data['sec']=self.sec
                positive_model_data['instance_size']=self.instance_size
                positive_model_data['VolumeSize']= self.VolumeSize
                positive_model_data['ssh_key']=self.ssh_key
                return positive_model_data
    def create_ec2(self):
        ec2 = boto3.resource('ec2')
        instance = ec2.create_instances(ImageId = self.download_old_ami_name()['ami']
            ,MinCount = 1
            ,MaxCount = 1,
            InstanceType = self.instance_size,
            BlockDeviceMappings=[
            {
                'DeviceName': '/dev/sda1',

                'Ebs': {
                    
                    'VolumeSize':  self.VolumeSize,
                },
            },
        ],
        UserData= '''#!/bin/bash
                    sudo apt-get install -y nginx > /tmp/hello''',
        KeyName = 'greyv19',
        SecurityGroupIds=[
                    self.sec,
                ]
                )
        host = instance[0]
        print(host.id)
        ec2 = boto3.client('ec2')
        ec2.create_tags(Resources=[host.id], Tags=[{'Key':'Name', 'Value':'dev_'+self.tag+''}])
        host.wait_until_running()
        host = ec2.Instance(host.id)
        return host

    def deploy_rails(self,host):
            print(host.public_ip_address)
            while self.get_by_name():
                time.sleep(60)
                self.create_ec2()
            try:
                std_out=subprocess.check_output('cd '+self.path+' && yarn',stderr=subprocess.STDOUT,shell=True)
                std_out=subprocess.check_output('cd '+self.path+' && bundle',stderr=subprocess.STDOUT,shell=True)
                std_out=subprocess.check_output('cd '+self.path+' && ./bin/webpack',stderr=subprocess.STDOUT,shell=True)
                std_out=subprocess.check_output('cd '+self.path+' &&  bundle exec cap production deploy server='+self.main_ip,stderr=subprocess.STDOUT,shell=True)
                std_out=subprocess.check_output('cd '+self.path+' &&  bundle exec cap production deploy server='+host.public_ip_address,stderr=subprocess.STDOUT,shell=True)
                std_out=subprocess.check_output('cd '+self.path+' && git stash',stderr=subprocess.STDOUT,shell=True)
                old=self.download_old_ami_name()
                image,name=self.image_create(host.id,old)
                self.delete(host.id)
                self.upload_new_ami(image,name,old)
                self.amidel(old)
            except subprocess.CalledProcessError as e:
                        print(str(e.output))
                        self.delete(host.id)
                        
    def image_create(self,instance_id,old):
                ec2 = boto3.resource('ec2')
                host = ec2.Instance(instance_id)
                newname=self.versionname(old['Image_name'])
                image = host.create_image(Name=newname)
                print(image.id)
                self.image_wait(image.id)
                return image.id,newname
    def image_wait(self,image):
                ec2 = boto3.client('ec2')
                waiter = ec2.get_waiter('image_available')
                waiter.wait(ImageIds=[image.id])

    def delete(self,id):
            ec2 = boto3.client('ec2')
            ec2.terminate_instances(InstanceIds=[id])


    
    def get_by_name(self):
            ec2 = boto3.client('ec2')
            filters = [{
                    'Name': 'tag:Name',
                    'Values': ['scale_'+self.tag]
                        },
                        {
                    'Name': 'instance-state-name',
                    'Values': ['running','pending']
                        },
                        ]
            reservations = ec2.describe_instances(Filters=filters)
            #print(reservations['Reservations'][0]['Instances'])
            if len(reservations['Reservations'])>=1:
                return True
            else:
                return False


    def amidel(self,old):
        ami=old['ami']
        ec2 = boto3.client('ec2')
        snapid=ec2.describe_images(ImageIds=[ami])['Images'][0]['BlockDeviceMappings'][0]['Ebs']['SnapshotId']
        response = ec2.deregister_image(
            ImageId=ami
        )
        self.snapdel(snapid)

    def snapdel(self,snapid):
        ec2 = boto3.client('ec2')
        response = ec2.delete_snapshot(
        SnapshotId=snapid
        )

    def upload_new_ami(self,ami,newname,example):

            print(example)
            example['ami']=ami
            example['version']=self.versionname(example['version'])
            pickle_out = open(self.file,"wb")
            pickle.dump(example, pickle_out)
            pickle_out.close()
            bucketName=self.bucket
            Key=self.file
            outPutname=self.file
            s3 = boto3.client('s3')
            s3.upload_file(Key,bucketName,outPutname)

    def versionname(self,name):
            name,ver, rev = str(name).split('.')
            versionname=name+'.'+ver + '.' + str(int(rev)+1) 
            return versionname
    def test_run(self):
        client = docker.from_env()    
        volume={self.path: {'bind': '/root/webapp/'}}
        try :  
            client.containers.run('chiruuzumaki/rails_run_cases', 'bash /root/run.sh',volumes=volume)
        except ContainerError as e:
            print(e.stderr)
    def display(self):
        print(self.bucket)
        print(self.sec)
        print(self.path)
        print(self.file)
        print(self.main_ip)
        print(self.instance_size)
        print(self.VolumeSize)
        print(self.tag)

    def execall(self):
        self.display()
        host=self.create_ec2()
        self.deploy_rails(host)
        pass
        